
import subprocess
from dataclasses import dataclass
from pathlib import Path
from shlex import split
from typing import List

import requests
import yt_dlp
from bs4 import BeautifulSoup

all_course_link = [
    "https://kodekloud.com/courses/docker-for-the-absolute-beginner/",
]


@dataclass
class Lesson:
    name: str
    url: str
    is_video: bool


@dataclass
class Topic:
    name: str
    lessons: List[Lesson]

    @classmethod
    def make(cls, topic):
        title = topic.find("div", class_="ld-item-title")
        name = title.span.text.strip()

        lessons_urls = topic.find_all("a", class_="ld-topic-row")
        lessons_names = topic.find_all("span", class_="ld-topic-title")

        lessons = []

        for lesson_url, lesson_name in zip(lessons_urls, lessons_names):
            is_video = "video_topic_enabled" in lesson_name["class"]
            lessons.append(Lesson(name=lesson_name.text.strip(), url=lesson_url["href"], is_video=is_video))

        return Topic(name=name, lessons=lessons)


# def download_video(url, output_path):
#     yt_dlp.utils.std_headers[
#         "Cookie"
#     ] = """"""
#     ydl_opts = {
#         "concurrent_fragment_downloads": 15,
#         "outtmpl": f"{output_path}.%(ext)s",
#     }
#     with yt_dlp.YoutubeDL(ydl_opts) as ydl:
#         ydl.download([url])

COOKIE = """<<<login cookie>>>"""


def download_video(url, output_path):
    command = f"yt-dlp -N 15 --add-header 'Cookie:{COOKIE}' -o '{output_path}' {url}"
    process = subprocess.Popen(split(command))
    return process.wait()


for course_link in all_course_link:
    page = requests.get(course_link)
    soup = BeautifulSoup(page.content, "html.parser")

    topics = soup.find_all("div", class_="ld-item-list-item")
    course_title = soup.find("h1", class_="entry-title").text.strip()
    items = []
    for topic in topics:
        items.append(Topic.make(topic))

    for i, item in enumerate(items, start=1):
        for j, lesson in enumerate(item.lessons, start=1):
            if lesson.is_video:
                file_path = Path(
                    Path(r"<<<download path>>>")
                    / course_title.replace(":", "- ")
                    / f"{i} - {item.name}"
                    / f"{j} - {lesson.name.replace('/', '_')}.mp4"
                )
                file_path.parent.mkdir(parents=True, exist_ok=True)
                download_video(lesson.url, file_path)
