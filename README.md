1. Edit "Course link , download path" & "login cookie" in main.py

2. login cookie -- open one of the course video and inspect this. 
                network > Response Headers > cookie:

```python
python3 -m venv .venv
source .venv /bin/activate
pip install requests yt-dlp beautifulsoup4
python main.py
```

If shown error like that
> WARNING: You have requested merging of multiple formats but ffmpeg is not installed. The formats won't be merged

Please install this
```bash
sudo apt install ffmpeg 
```